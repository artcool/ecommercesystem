package com.rydata.lottery.service;

import com.github.pagehelper.PageInfo;
import com.rydata.lottery.common.ServerResponse;
import com.rydata.lottery.pojo.Product;
import com.rydata.lottery.vo.ProductDetailVo;
import org.springframework.stereotype.Service;

@Service
public interface IProductService {

    ServerResponse saveOrUpdateProduct(Product product);

    ServerResponse<String> setSaleStatus(Integer productId,Integer status);

    ServerResponse<ProductDetailVo> manageProductDetail(Integer productId);

    ServerResponse getProductList(int pageNum,int pageSize);

    ServerResponse<PageInfo> searchProduct(String productName, Integer productId, int pageNum, int pageSize);

    ServerResponse<ProductDetailVo> getProductDetail(Integer productId);

    ServerResponse<PageInfo> getProductByKeywordCategoryId(String keyword,
                                                           Integer categoryId,
                                                           int pageNum,
                                                           int pageSize,
                                                           String orderBy);
}
