package com.rydata.lottery.service;

import com.rydata.lottery.common.ServerResponse;
import com.rydata.lottery.pojo.User;
import org.springframework.stereotype.Service;

@Service
public interface IUserService {

    ServerResponse<User> login(String username, String password);

    ServerResponse<String> register(User user);

    ServerResponse<String> checkValid(String str,String type);

    ServerResponse selectQuestion(String username);

    ServerResponse<String> checkAnswer(String username,String question,String answer);

    ServerResponse<String> forgetResetPassword(String username,String passwordNew,String forgetToken);

    ServerResponse<String> resetPassword(String passwordOld,String passwordNew,User user);

    ServerResponse<User> updatePersonalInfo(User user);

    ServerResponse<User> getPersonalInfo(Integer userId);

    ServerResponse checkAdminRole(User user);
}
