package com.rydata.lottery.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件处理服务
 */
@Service
public interface IFileService {

    String upload(MultipartFile file, String path);
}
