package com.rydata.lottery.service;


import com.github.pagehelper.PageInfo;
import com.rydata.lottery.common.ServerResponse;
import com.rydata.lottery.pojo.Shipping;

public interface IShippingService {

    ServerResponse addShipping(Integer userId, Shipping shipping);

    ServerResponse<String> deleteShipping(Integer userId,Integer shippingId);

    ServerResponse updateShipping(Integer userId, Shipping shipping);

    ServerResponse<Shipping> selectShipping(Integer userId,Integer shippingId);

    ServerResponse<PageInfo> list(Integer userId, int pageNum, int pageSize);
}
