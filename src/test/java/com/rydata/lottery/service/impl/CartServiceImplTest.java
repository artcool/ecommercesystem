package com.rydata.lottery.service.impl;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Random;

import static org.junit.Assert.*;

public class CartServiceImplTest {

    @Test
    public void test1() {
        //java基于浮点型计算的丢失精度问题
        System.out.println(0.05 + 0.01);
        System.out.println(1.0 - 0.42);
        System.out.println(4.015*100);
        System.out.println(123.3/100);
    }

    @Test
    public void testBigDec() {
        BigDecimal b1 = new BigDecimal(0.05);
        BigDecimal b2 = new BigDecimal(0.01);
        System.out.println(b1.add(b2));
    }

    @Test
    public void testBigDec2() {
        BigDecimal b1 = new BigDecimal(0.05+"");
        BigDecimal b2 = new BigDecimal(0.01+"");
        System.out.println(b1.add(b2));
    }

    @Test
    public void testFront() {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            int v = (int) (Math.random() * 35 + 1);
            list.add(v);
        }

        System.out.println(list);
    }

    @Test
    public void testBack() {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            int v = (int) (Math.random() * 12 + 1);
            list.add(v);
        }
        System.out.println(list);
    }
}