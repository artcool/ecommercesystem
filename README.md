# EcommerceSystem

基于java的电商系统

###环境搭建
* jdk
	export JAVA_HOME=/usr/java/jdkxxx
	
* tomcat
	export CATALINA=/youdir/xxx
	
* maven
	export MAVEN_HOME=/yourdir/apache-maven-xxx
	
* vsftpd
	
	-- 简介
		一个完全免费的、开放源代码的ftp服务器软件
		
	-- 特点
		小巧轻快，安全易用，支持虚拟用户、支持带宽限制等功能
		
	-- 安装
		执行 yum -y install vsftpd
		rpm -qa | grep vsftpd 检查是否已经安装
		默认配置文件在/etc/vsftpd/vsftpd.conf
	
	-- 创建虚拟用户
		‘ 选择在跟或者用户目录下创建ftp文件夹：mkdir ftpfile
		‘ 添加匿名用户：useradd ftpuser -d /ftpfile -s /sbin/nologin
		‘ 修改ftpfile权限：chown -R ftpuser.ftpuser /ftpfile
		‘ 重设ftpuser密码：passwd ftpuser
		
	-- 配置
		‘ cd /etc/vsftpd
		‘ sudo vi chroot_list
		‘ 把刚才新增的虚拟用户添加到此配置文件中，后续要引用
		‘ 保存退出
		‘ sudo vi /etc/selinux/config,修改为SELINUX=disable
		‘ 保存退出	reboot（重启服务器）
		’ sudo vi /etc/vsftpd/vsftpd.conf
		
	-- 防火墙配置
		‘ sudo vi /etc/sysconfig/iptables
		' -A INPUT -p TCP --dport 61001:62000 -j ACCEPT
		-A OUTPUT -p TCP --sport 61001:62000 -j ACCEPT
		-A INPUT -p TCP --dport 20 -j ACCEPT
		-A OUTPUT -p TCP --sport 20 -j ACCEPT
		-A INPUT -p TCP --dport 21 -j ACCEPT
		-A OUTPUT -p TCP --sport 21 -j ACCEPT 添加到防火墙配置中
		’ sudo service iptables restart 执行命令重启防火墙
		
	-- 验证
		‘ sudo service vsftpd restart 重启 
		’ 打开浏览器访问：ftp://ipaddress
		
	
* 公共环境变量
	Linux系统环境配置文件：/etc/profile
	export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt/jar:$JAVA_HOME/lib/tools.jar
	export PATH=$JAVA_HOME/bin:$MAVEN_HOME/bin:$PATH
	
	配置文件生效命令：source /etc/profile
	
